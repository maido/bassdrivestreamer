package com.bassdrive.streamer.show;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bassdrive.streamer.ArchiveListLoader;
import com.bassdrive.streamer.ArchiveFragment.OnArchiveFileSelectedListener;

import android.os.Parcel;
import android.util.Log;

public class ArchiveItem implements Item {

	private final static Pattern p = Pattern.compile(".*/(.*)/");

	protected String url;

	protected ArchiveItem(Parcel parcel) {
		this.url = parcel.readString();
	}

	public ArchiveItem(String url) {
		this.url = url;
	}

	public void handleClick(ArchiveListLoader loader,
			OnArchiveFileSelectedListener listener) {
		Log.d("ArchiveItem", "Selecting archive item " + url);
		loader.setActive(this);
	}

	@Override
	public String getURL() {
		return url;
	}

	@Override
	public void getInfo(OnInfoCallback onInfoCallback) {
		onInfoCallback.onInfoCallback(getInfo());
	}

	protected Info getInfo() {
		Matcher matcher = p.matcher(escape(url));
		if (matcher.matches()) {
			return new Info(matcher.group(1));
		}
		return new Info(url);
	}

	@Override
	public String toString() {
		return getInfo().toString();
	}

	protected String escape(String input) {
		String info;
		try {
			info = URLDecoder.decode(input, Charset.defaultCharset().name());
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		return info;
	}

}
