package com.bassdrive.streamer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;
import com.bassdrive.streamer.PlayerService.LocalBinder;
import com.bassdrive.streamer.PlayerService.OnPlayerServicePrepared;
import com.bassdrive.streamer.PlayerService.OnTrackInfoChanged;
import com.bassdrive.streamer.show.Info;
import com.bassdrive.streamer.show.Item.OnInfoCallback;
import com.bassdrive.streamer.show.Playable;
import com.bassdrive.streamer.view.NowPlayingView;
import com.bassdrive.streamer.view.PlayerSeekBar;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_player)
public class PlayerFragment extends SherlockFragment implements OnPlayerServicePrepared, OnTrackInfoChanged {

	private static final String TAG = "PlayerFragment";

	@ViewById(R.id.liveButton)
	Button live;

	@ViewById(R.id.stopButton)
	Button stop;

	@ViewById(R.id.playerSeekBar)
	PlayerSeekBar seekbar;

	@ViewById(R.id.nowPlaying)
	NowPlayingView nowPlaying;

	private PlayerService playerService;

	private boolean bound;

	@Override
	public void onStart() {
		super.onStart();
		Intent intent = new Intent(getActivity(), PlayerService.class);
		getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onStop() {
		super.onStop();
		// Unbind from the service
		if (bound) {
			playerService.removeOnPreparedListener();
			playerService.removeOnTrackInfoChangedListener();
			getActivity().unbindService(mConnection);
			bound = false;
		}
	}

	@Click(R.id.stopButton)
	void stopClicked() {
		Log.d(TAG, "Stop clicked");
		seekbar.reset();
		if (bound) {
			playerService.stop();
		}
		PlayerService.stop(getActivity());
		Button play = (Button) getView().findViewById(R.id.play);
		play.setText(R.string.pause);
		play.setEnabled(false);
	}

	@Click(R.id.liveButton)
	void liveClicked() {
		Log.d(TAG, "Live clicked");
		PlayerService.playLive(getActivity());
	}

	private final ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			LocalBinder binder = (LocalBinder) service;
			playerService = binder.getService();
			playerService.setOnPreparedListener(PlayerFragment.this);
			playerService.setOnTrackInfoChangedListener(PlayerFragment.this);
			bound = true;
			if (playerService.isPlaying()) {
				initPlayer();
			} else {
				Log.d(TAG, "Player is not running!");
			}
			Log.d(TAG, "Service is bound");
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			bound = false;
		}
	};

	@Override
	public void onPlayerServicePrepared(MediaPlayer mp) {
		initPlayer();
		Log.d(TAG, "Player prepared");
	}

	private void initPlayer() {
		Button play = (Button) getView().findViewById(R.id.play);
		play.setEnabled(true);
		playerService.initSeekBar(seekbar);
	}

	@Click(R.id.play)
	void playClicked(View v) {
		Log.d(TAG, "Play clicked");
		if (playerService.isPlaying()) {
			playerService.pause();
			((Button) v).setText(R.string.play);
		} else {
			playerService.start();
			((Button) v).setText(R.string.pause);
		}
	}

	@Override
	public void onTrackInfoChanged(MediaPlayer mp, Playable item) {
		if (item != null) {
			item.getInfo(new OnInfoCallback() {

				@Override
				public void onInfoCallback(Info info) {
					nowPlaying.setText(info.toString());
				}
			});
		} else {
			nowPlaying.setText(R.string.track);
		}
	};

}