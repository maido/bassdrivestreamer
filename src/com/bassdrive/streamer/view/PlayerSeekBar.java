package com.bassdrive.streamer.view;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.bassdrive.streamer.R;
import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.player_seekbar)
public class PlayerSeekBar extends LinearLayout {

	@ViewById
	SeekBar seekBar;
	@ViewById
	TextView current;
	@ViewById
	TextView duration;

	public PlayerSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setProgress(int progress) {
		seekBar.setProgress(progress);
		current.setText(formatMillis(progress));
	}

	public void setMax(int max) {
		seekBar.setMax(max);
		duration.setText(formatMillis(max));
	}

	public void setInfinity() {
		seekBar.setMax(0);
		duration.setText(getResources().getString(R.string.infinity));
	}

	public void reset() {
		setInfinity();
		setProgress(0);
		seekBar.setOnSeekBarChangeListener(null);
	}

	public void setOnSeekBarChangeListener(OnSeekBarChangeListener listener) {
		seekBar.setOnSeekBarChangeListener(listener);
	}

	private String formatMillis(int millis) {
		return DateUtils.formatElapsedTime(millis / 1000);
	}

}
