package com.bassdrive.streamer.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bassdrive.streamer.R;
import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.now_playing)
public class NowPlayingView extends LinearLayout {

	@ViewById
	TextView track;

	public NowPlayingView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setText(String text) {
		track.setText(text);
	}

	public void setText(int id) {
		track.setText(id);
	}

}
