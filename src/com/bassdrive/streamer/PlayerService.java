package com.bassdrive.streamer;

import java.io.IOException;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.bassdrive.streamer.show.Info;
import com.bassdrive.streamer.show.Item.OnInfoCallback;
import com.bassdrive.streamer.show.LiveStream;
import com.bassdrive.streamer.show.Playable;
import com.bassdrive.streamer.show.Show;
import com.bassdrive.streamer.view.PlayerSeekBar;

public class PlayerService extends Service implements MediaPlayerControl, OnCompletionListener, OnPreparedListener, OnSeekBarChangeListener {

	public interface OnPlayerServicePrepared {
		void onPlayerServicePrepared(MediaPlayer mp);
	}

	public interface OnTrackInfoChanged {
		void onTrackInfoChanged(MediaPlayer mp, Playable item);
	}

	private static final String ITEM = "PlayerService.ITEM";

	private static final String TAG = "PlayerService";

	private final IBinder binder = new LocalBinder();

	private MediaPlayer mediaPlayer;

	private OnPlayerServicePrepared listener;

	private OnTrackInfoChanged onTrackInfoChanged;

	private boolean prepared;

	private Playable trackInfo;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Playable i = intent.getParcelableExtra(ITEM);
		if (!i.equals(trackInfo)) {
			play(i);
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onCreate() {
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
		mediaPlayer.setOnPreparedListener(this);
		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) {
		if (onTrackInfoChanged != null) {
			onTrackInfoChanged.onTrackInfoChanged(mediaPlayer, trackInfo);
		}
		return binder;
	}

	class LocalBinder extends Binder {
		PlayerService getService() {
			return PlayerService.this;
		}
	}

	public void setOnPreparedListener(OnPlayerServicePrepared listener) {
		this.listener = listener;
	}

	public void removeOnPreparedListener() {
		this.listener = null;
	}

	public void setOnTrackInfoChangedListener(OnTrackInfoChanged listener) {
		this.onTrackInfoChanged = listener;
		if (onTrackInfoChanged != null) {
			onTrackInfoChanged.onTrackInfoChanged(mediaPlayer, trackInfo);
		}
	}

	public void removeOnTrackInfoChangedListener() {
		this.listener = null;
	}

	@Override
	public boolean canPause() {
		return !trackInfo.isLive();
	}

	@Override
	public boolean canSeekBackward() {
		return !trackInfo.isLive();
	}

	@Override
	public boolean canSeekForward() {
		return !trackInfo.isLive();
	}

	@Override
	public int getAudioSessionId() {
		return 0;
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public int getCurrentPosition() {
		return mediaPlayer.getCurrentPosition();
	}

	@Override
	public int getDuration() {
		return mediaPlayer.getDuration();
	}

	@Override
	public boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}

	@Override
	public void pause() {
		mediaPlayer.pause();
	}

	@Override
	public void seekTo(int pos) {
		mediaPlayer.seekTo(pos);
	}

	@Override
	public void start() {
		Log.d(TAG, "Starting the player");
		if (prepared) {
			Log.d(TAG, "Resuming playback");
			mediaPlayer.start();
		} else {
			play(new LiveStream());
		}
	}

	public static void playLive(Context context) {
		Intent i = new Intent(context, PlayerService.class);
		i.putExtra(ITEM, new LiveStream());
		context.startService(i);
	}

	public static void play(Context context, Show item) {
		Intent i = new Intent(context, PlayerService.class);
		i.putExtra(ITEM, item);
		context.startService(i);
	}

	public static void stop(Context context) {
		Intent i = new Intent(context, PlayerService.class);
		context.stopService(i);
	}

	private void play(Playable item) {
		Log.d(TAG, "Starting playback");
		mediaPlayer.reset();
		setTrackInfo(item);
		try {
			mediaPlayer.setDataSource(item.getURL());
			mediaPlayer.prepareAsync();
		} catch (IOException e) {
			Log.e(TAG, "Could not open file " + item.getURL() + " for playback.", e);
		}
	}

	public void stop() {
		mediaPlayer.setOnBufferingUpdateListener(null);
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		setTrackInfo(null);
		stopForeground(true);
	}

	private void startOnForeground(Info info) {
		Intent intent = new Intent(getApplicationContext(), MainActivity_.class);
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		Notification notification = createNotification(info, pi);
		startForeground(2, notification);
	}

	private Notification createNotification(Info info, PendingIntent pi) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
		builder.setTicker(info.toString());
		builder.setOngoing(true);
		builder.setSmallIcon(android.R.drawable.ic_media_play);
		builder.setContentIntent(pi);
		builder.setContentTitle("Bassdrive");
		builder.setContentText(info.toString());
		Notification notification = builder.getNotification();
		return notification;
	}

	@Override
	public void onDestroy() {
		if (mediaPlayer != null) {
			mediaPlayer.release();
		}
		removeOnPreparedListener();
		removeOnTrackInfoChangedListener();
		super.onDestroy();
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		Log.d(TAG, "Releasing player");
		if (mp != null) {
			mp.release();
		}
		prepared = false;
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		prepared = true;
		Log.d(TAG, "Player prepared");
		if (listener != null) {
			listener.onPlayerServicePrepared(mediaPlayer);
		}
		trackInfo.getInfo(new OnInfoCallback() {

			@Override
			public void onInfoCallback(Info info) {
				startOnForeground(info);
			}
		});
		mp.start();
	}

	public void initSeekBar(final PlayerSeekBar seekBar) {
		Log.d(TAG, "Inited seekbar");
		seekBar.setProgress(mediaPlayer.getCurrentPosition());
		if (!trackInfo.isLive()) {
			Log.d(TAG, "Is not live");
			seekBar.setMax(mediaPlayer.getDuration());
			seekBar.setOnSeekBarChangeListener(this);
		} else {
			seekBar.setInfinity();
		}
		mediaPlayer.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {

			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				seekBar.setProgress(mp.getCurrentPosition());
			}
		});
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (!trackInfo.isLive() && fromUser) {
			seekTo(progress);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	private void setTrackInfo(Playable item) {
		this.trackInfo = item;
		if (onTrackInfoChanged != null) {
			onTrackInfoChanged.onTrackInfoChanged(mediaPlayer, trackInfo);
		}
	}

	public void pausePlayer() {

	}

}
