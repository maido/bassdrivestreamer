package com.bassdrive.streamer;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.bassdrive.streamer.show.ArchiveItem;
import com.bassdrive.streamer.show.Show;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

public class ArchiveListLoader extends AsyncTaskLoader<List<ArchiveItem>> {

	private static final String ARCHIVE_URL = "http://archives.bassdrivearchive.com/";

	private static final String TAG = "ShowLoader";

	private final String baseURL;

	private List<ArchiveItem> shows;

	private final int position;

	private ArchiveItem active;

	public ArchiveListLoader(Context context, int position) throws URISyntaxException {
		super(context);
		this.position = position;
		this.baseURL = getArchiveURL();
		Log.d(TAG, "Loader created: " + position);
	}

	@Override
	public List<ArchiveItem> loadInBackground() {
		Log.d(TAG, "Loading in background");
		List<ArchiveItem> shows = new ArrayList<ArchiveItem>();
		try {
			String url = getStreamURL();
			Log.d(TAG, "Getting items from " + url);
			Document doc = Jsoup.connect(url).get();
			Elements elems = doc.select("ul li a");
			for (Element element : elems) {
				String value = element.text();
				if (!isParent(value)) {
					shows.add(getArchiveItem(value));
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "Failed to load shows", e);
			e.printStackTrace();
		}
		return shows;
	}

	private ArchiveItem getArchiveItem(String value) throws MalformedURLException, URISyntaxException {
		if (isParent(value)) {
			return new ArchiveItem(getItemURL(value));
		} else if (value.endsWith(".mp3")) {
			return new Show(getItemURL(value));
		}
		return new ArchiveItem(getItemURL(value));
	}

	private String getStreamURL() throws URISyntaxException {
		return new URI(null, getItemURL(""), null).toASCIIString();
	}

	private String getItemURL(String path) {
		if (active != null) {
			return active.getURL().concat(path);
		}
		return baseURL.concat(path);
	}

	public static boolean isParent(String value) {
		return value.trim().equalsIgnoreCase("parent directory");
	}

	private String getArchiveURL() throws URISyntaxException {
		String[] weekdays = new DateFormatSymbols().getWeekdays();
		return ARCHIVE_URL + position + " - " + (weekdays[(position % 7) + 1]) + "/";
	}

	@Override
	public void deliverResult(List<ArchiveItem> data) {
		if (isReset()) {
			return;
		}
		shows = data;
		if (isStarted()) {
			super.deliverResult(data);
		}
	}

	@Override
	protected void onStartLoading() {
		if (shows != null) {
			deliverResult(shows);
		}
		if (takeContentChanged() || shows == null) {
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		cancelLoad();
	}

	@Override
	protected void onReset() {
		onStopLoading();
		if (shows != null) {
			shows = null;
		}
	}

	public void setActive(ArchiveItem item) {
		this.active = item;
	}

	public ArchiveItem getItem(int index) {
		return shows.get(index);
	}

}
